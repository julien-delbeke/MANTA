#include "Object.hpp"

Object::Object(int weight,bool isMovable,int stimuliType,int stimuliPower) : _numberToMove(weight), _movable(isMovable), _myPosition(nullptr), _rootStimuli(),_weight(),_stimuliType(stimuliType),_stimuliPower(stimuliPower){}

Object::Object(bool isMovable) :  _numberToMove(-1),_movable(isMovable), _myPosition(nullptr), _rootStimuli(),_weight(),_stimuliType(0),_stimuliPower(0){}

Object::Object(int weight,bool isMovable,Square* myPos,int stimuliType,int stimuliPower) : _numberToMove(weight), _movable(isMovable), _myPosition(myPos), _rootStimuli(),_weight(),_stimuliType(stimuliType),_stimuliPower(stimuliPower){}


int Object::getNumberToMove(){
	return _numberToMove;
}
bool Object::getMovability(){
	return _movable;
}
Square* Object::getPos(){
 return _myPosition;
}
void Object::setPos(Square* pos){
	_myPosition=pos;
	auto temp =_rootStimuli.lock();
	if(temp)
		temp->remove();
}
void Object::toBeCarried(){
	auto rootStimuli=_rootStimuli.lock();
	if(rootStimuli){
		rootStimuli->remove();
	}
	_myPosition=nullptr;
}
void Object::putOn(Square* pos){
	setPos(pos);
 }

 int Object::getWeight(){
 	return _weight;
 }

void Object::setStimuliType(int stimuliType){
	_stimuliType=stimuliType;
}
void Object::incStimuliType(){
	_stimuliType++;
}

void Object::squareFreeStimuli(int stimuliType){
	if(_stimuliType==stimuliType){
		if(!_rootStimuli.use_count()){
			normalStimuli();
		}
	}

}
void Object::normalStimuli(){
	auto temp=_rootStimuli.lock();
	if(temp){
		temp->remove();
	}
	if(_stimuliType!=-1){
		temp=std::make_shared<Stimuli>(new Stimuli(_stimuliType,_stimuliPower,_myPosition,this));
		if(_myPosition){
			temp->propagationStimuli(temp);
		}
		_rootStimuli=temp;
	}
}
Object::~Object(){
	_myPosition=nullptr;
	_rootStimuli=std::make_shared<Stimuli>(new Stimuli());;
}