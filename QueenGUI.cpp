#include "QueenGUI.hpp"

QueenGUI::QueenGUI(QGridLayout* fenetre,int range,int turnHungry, int nbrDeathEating,int frequenMove ,int turnDeath,int fregEgg,Square* Pos, std::string queenImage,int nbrEgg,int maxAlea): ObjectGUI(QString(queenImage.c_str())),
						Queen(range,turnHungry,nbrDeathEating,frequenMove,turnDeath,fregEgg,Pos,nbrEgg,maxAlea),anim(nullptr),grid(fenetre),_annimeDone(true){
    _label = new QLabel();
    _label->setFixedSize(SIZEQUEEN,SIZEQUEEN);
    _label->setScaledContents(true);
    _label->setStyleSheet("background:transparent;");
    _label->setPixmap(_pic);
    fenetre->addWidget(_label,_myPosition->getPosI(),_myPosition->getPosJ());

}

bool QueenGUI::moveSquare(Square* next){
    if(Queen::moveSquare(next)){
        _annimeDone=false;
        emit moveSignal(next);
        return true;
    }
    return false;
}

void QueenGUI::moveSlot(Square* next){
	if(anim){
		delete anim;
	}
    anim = new QPropertyAnimation(getLabel(),"geometry");
    SquareGUI* nextGUI(dynamic_cast<SquareGUI*>(next));
    if(nextGUI  and grid->indexOf(getLabel())){
        QLabel *temp = nextGUI->getLabel();
        delete grid->takeAt(grid->indexOf(getLabel()));
       
       
        anim->setStartValue(QRect(getLabel()->x(),getLabel()->y(),0,0));
        anim->setEndValue(QRect(temp->x()+1,temp->y()+1,0,0));
        anim->setDuration(DURATION);
        QObject::connect(anim,SIGNAL(finished()),this,SLOT(endAnimation()));
        anim->start();
    }
}

void QueenGUI::endAnimation(){
    if(_myPosition){
    	grid->addWidget(_label,_myPosition->getPosI(),_myPosition->getPosJ());
	}
    _annimeDone=true;
}
bool QueenGUI::isReady(){
	return _annimeDone;
}

void QueenGUI::death(){
    _mtx.lock();
    Queen::death();
    emit deathSignal();

}

void QueenGUI::deathSlot(){
    _namePix="skull.png";
    _pic=QPixmap(_namePix);
    _label->setPixmap(_pic);
    _mtx.unlock();
}