#include "Queen.hpp"

Queen::Queen(int range,int turnHungry, int nbrDeathEating,int frequenMove ,int turnDeath,int fregEgg,Square* pos,int nbrEgg,int maxAlea):ActiveAgent(0,range,turnHungry,nbrDeathEating,frequenMove,turnDeath,pos,9)
								,_freqEgg(fregEgg),_inHisRoom(inARoom(15)),_newGeneration(0),_nbrEgg(nbrEgg),_maxAlea(maxAlea),_stayInPlace(MOVEINHISROOM){
	srand (time(nullptr));
	if(_inHisRoom){
		_stayInPlace=0;
	}
	auto temp=std::make_shared<Stimuli>(new Stimuli(_stimuliType,4,_myPosition,this));
	_weight=15;
	temp->propagationStimuli(temp);
	_rootStimuli=temp;
}

void Queen::display(){
	if(isAlive())
		std::cout<<" Q ";
	else{
		std::cout<<"MOR";
	}
}

void Queen::act(){
	if(isAlive()){
		incTurn();
		if(!(_turnEating%getTurnHungry())){
			hungry();
		}
		if(_turn==_turnDeath){
			death();
		}
		if(isAlive()){
			if(!(_turn%_frequenMove)and _stayInPlace){
				if(!_inHisRoom)
					randomMove();
				else{
					stayInRoom();
					_stayInPlace--;
				}
			}
			if(!(_turn%_freqEgg) and !_stayInPlace){
				layEgg();
			}
		}
	}
}
void Queen::stayInRoom(){
	if(!_path.size()){
		if(_inHisRoom and _stayInPlace==MOVEINHISROOM){
			moveAwayDoor();
		}
		else{
			goToWall();
		}
	}
	goToObjective();
}
void Queen::layEgg(){
	int nbrEgg=_nbrEgg+ (rand() %( _maxAlea))+ 1;
	_newGeneration=nbrEgg;
}
bool Queen::haveEgg(){
	return _newGeneration;
}
int Queen::getNewGeneration(){
	int temp=_newGeneration;
	_newGeneration=0;
	return temp;
}
bool Queen::moveSquare(Square* next){
	if(ActiveAgent::moveSquare(next)){
		_inHisRoom=inARoom(15);
		normalStimuli();
		return true;
	}
	return false;
}
bool Queen::isReady(){
	return true;
}