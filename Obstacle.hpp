#ifndef Obstacle_h
#define Obstacle_h

#include "Object.hpp"

class Obstacle : public Object{
public:
	Obstacle(bool);
	~Obstacle()=default;
};
#endif