#ifndef MYTHREAD_HPP
#define MYTHREAD_HPP

#include "WorldGUI.hpp"
#include <QThread>


class MyThread : public QThread{
     Q_OBJECT
public:
    MyThread(WorldGUI*);
    void astop();
protected:
     void run();
private:
    WorldGUI *_world;
};
 
#endif