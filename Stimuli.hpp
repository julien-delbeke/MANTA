#ifndef MACROOBJECTIVE
#define MACROOBJECTIVE

#define NORMAL 0
#define CAREANT 1
#define CAREEGG 10
#define CARELARVA 2
#define CAREPUPA 3
#define FEEDINGLARVA 4
#define FEEDING 5
#define CARRYINGEGG 6
#define CARRYINGLARVA 7
#define CARRYINGPUPA 8
#define CARRYINGFOOD 9
#endif

#ifndef STIMULI_HPP
#define STIMULI_HPP

class Square;
class Object;
#include <memory>
#include <vector>
#include <queue>

class Stimuli{
	public:
		Stimuli();
		Stimuli(Stimuli&,Square*);
		Stimuli(Stimuli*);
		Stimuli(const Stimuli&)=delete;
		Stimuli operator=(const Stimuli&)=delete;
		Stimuli(int,float,Square*,Object*);
		bool isBiger(Stimuli const&) const;
		bool isEgal(Stimuli const&)const;
		int getType()const;
		float getPower()const;
		bool notSameRoot(Stimuli*);
		Object* getRoot()const ;
		Square* getOrinin()const;
		bool operator!=(const Stimuli&);
		void propagationStimuli(std::shared_ptr<Stimuli>);
		void remove();
		void display();
		~Stimuli();
	protected:
		void propagationStimuliWork(std::shared_ptr<Stimuli>);

		int _type;
		float _power;
		Square* _origin;
		Object* _root;
		std::vector<std::weak_ptr<Stimuli>> _myPropagation;

};

bool operator>(Stimuli const&, Stimuli const&);
bool operator>=(Stimuli const&, Stimuli const&);

#endif /* STIMULI_HPP */