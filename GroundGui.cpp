#include "GroundGui.hpp"

GroundGui::GroundGui(bool isMovable):Ground(isMovable),_ground_img(),_label(nullptr){
	_ground_img = "ground.jpg";
}

QLabel* GroundGui::returnImage(int pos_x,int pos_y){
	_label = new QLabel();
	_label->setGeometry(pos_x,pos_y,100,100);
	return _label;
}