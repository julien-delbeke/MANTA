#include "WallGUI.hpp"

WallGui::WallGui(bool isMovable) : Wall(isMovable),_map_img(),_label(nullptr){
	_map_img = "wall.png";
}
ClickableLabel* WallGui::returnImage(int pos_x,int pos_y){
	_label = new ClickableLabel();

	if((pos_x+pos_y)%14==0 or (pos_x+pos_y)%15==0){
		_label->setPixmap(QPixmap("water3.png"));
	}
	else if((pos_x+pos_y)%7==0 or (pos_x+pos_y)%8==0){
		_label->setPixmap(QPixmap("gravillon2.png"));
	}

	else {
		_label->setPixmap(QPixmap("stones1.png"));
	}
	return _label;
}
