#include "Agent.hpp"

Agent::Agent(int ,int range,int stimuliType,int deathStimuli,float deathStimuliPower):Object(0,true,stimuliType,5), _turn(0), _range(range), _isAlive(true),_deathStimuli(deathStimuli),_deathStimuliPower(deathStimuliPower) {}

Agent::Agent(int ,int range,Square* myPosition,int stimuliType,int deathStimuli,float deathStimuliPower):Object(0,true,myPosition,stimuliType,5), _turn(0), _range(range), _isAlive(true)
				,_deathStimuli(deathStimuli),_deathStimuliPower(deathStimuliPower){
	if(!_myPosition->addObjectOnSquare(this)){
		std::cout<<"AAAAAAAAA"<<std::endl;
		throw 1;
	}
}

void Agent::death(){
	_isAlive=false;
	_stimuliType=_deathStimuli;
	_stimuliPower=_deathStimuliPower;
	normalStimuli();
}

bool Agent::isAlive(){ return _isAlive; }

void Agent::kill(){ death(); }

void Agent::incTurn(){
	if(_myPosition)
		_turn++;
}

void Agent::toBeCarried(){
	_myPosition->removeObjectOnSquare(this);
	Object::toBeCarried();
}

void Agent::putOn(Square* pos){
	Object::putOn(pos);
	_myPosition->addObjectOnSquare(this);
}

void Agent::setPos(Square* pos){
	Object::setPos(pos);	
	normalStimuli();
}
bool Agent::isReady(){
	return true;
}