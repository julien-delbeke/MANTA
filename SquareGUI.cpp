#include "SquareGUI.hpp"

SquareGUI::SquareGUI(int posI,int posJ,bool typeSquare,int type,float stimuliResi,QGridLayout* window) : ObjectGUI(""),Square(posI,posJ,typeSquare,stimuliResi),_window(window){
	_label = new ClickableLabel();
	_label->setScaledContents(true);
	switch(type){
		case '7':
			_label->setPixmap(QPixmap("kev2.jpg"));
			break;
		case '0':
			if((posJ+posI)%14==0 or (posJ+posI)%15==0){
				_label->setPixmap(QPixmap("water3.png"));
			}
			else if((posJ+posI)%7==0 or (posJ+posI)%8==0){
				_label->setPixmap(QPixmap("gravillon2.png"));
			}
			else {
				_label->setPixmap(QPixmap("stones1.png"));
			}
			_label->setStyleSheet("background:transparent;");
			break;
		case '1':
			_label->setStyleSheet("background:BurlyWood;");
			QObject::connect(_label,SIGNAL(clicked()),this,SLOT(beClicked()));
			break;
		case '9':
			_label->setStyleSheet("background:BurlyWood;");
			QObject::connect(_label,SIGNAL(clicked()),this,SLOT(beClicked()));
			break;
		default:
			_label->setStyleSheet("background:transparent;");
			QObject::connect(_label,SIGNAL(clicked()),this,SLOT(beClicked()));
			break;
	}
	_label->setGeometry(0,0,100,100);
}

SquareGUI::SquareGUI(Object* Object,int posI,int posJ,bool typeSquare,int type,float stimuliResi,QGridLayout* window) :ObjectGUI(""),Square(Object,posI,posJ,typeSquare,stimuliResi),_window(window){
	_label = new ClickableLabel();
	_label->setScaledContents(true);
	switch(type){
		case '7':
			_label->setPixmap(QPixmap("kev2.jpg"));
			break;
		case '0':
			if((posJ+posI)%14==0 or (posJ+posI)%15==0){
				_label->setPixmap(QPixmap("water3.png"));
			}
			else if((posJ+posI)%7==0 or (posJ+posI)%8==0){
				_label->setPixmap(QPixmap("gravillon2.png"));
			}
			else {
				_label->setPixmap(QPixmap("stones1.png"));
			}
			_label->setStyleSheet("background:transparent;");
			break;
		case '1':
			_label->setStyleSheet("background:BurlyWood;");
			QObject::connect(_label,SIGNAL(clicked()),this,SLOT(beClicked()));
			break;
		case '9':
			_label->setStyleSheet("background:BurlyWood;");
			QObject::connect(_label,SIGNAL(clicked()),this,SLOT(beClicked()));
			break;
		default:
			_label->setStyleSheet("background:transparent;");
			QObject::connect(_label,SIGNAL(clicked()),this,SLOT(beClicked()));
			break;
	}
	_label->setGeometry(0,0,100,100);
}
void SquareGUI::beClicked(){
	emit clicked(this);
}

bool SquareGUI::addObjectOnSquare(Object * object){
	bool tempVal=Square::addObjectOnSquare(object);
	auto temp=dynamic_cast<ActiveAgent*>(object);
	if(tempVal and !(temp and temp->isAlive())){
		emit addGUIObject(object);
	}
	return tempVal;
}
void SquareGUI::removeObjectOnSquare(Object* object){
	Square::removeObjectOnSquare(object);
	auto temp=dynamic_cast<ActiveAgent*>(object);
	if(!(temp and temp->isAlive())){
		emit removeGUIObject(object);
	}
}

void SquareGUI::removeGUIObjectSlot(Object *object){
	_mtx.lock();
	auto temp=dynamic_cast<ObjectGUI*>(object);
	if(temp and _window->indexOf(temp->getLabel())){
		delete _window->takeAt(_window->indexOf(temp->getLabel()));
		temp->deleteLabel();
	}
	_mtx.unlock();

}

void SquareGUI::addGUIObjectSlot(Object *object){
	_mtx.lock();
	auto temp=dynamic_cast<ObjectGUI*>(object);
	if(temp){
		_window->addWidget(temp->getLabel(),getPosI(),getPosJ());
		temp->normalPic();
		
	}
	_mtx.unlock();
}