#ifndef Seed_h
#define Seed_h

#include "Object.hpp"
#include "Food.hpp"


class Seed : public Food{
public:
	Seed(int,bool,int);
	void display()override;
	virtual ~Seed()=default;
};
#endif