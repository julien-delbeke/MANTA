#ifndef SeedGUI_h
#define SeedGUI_h

#include "ObjectGUI.hpp"
#include <QPixmap>
#include "Seed.hpp"


class SeedGUI: public Seed, public ObjectGUI{
public:
	SeedGUI(int,bool,int,std::string);
	virtual ~SeedGUI()=default;
};



#endif
