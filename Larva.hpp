#ifndef Larva_HPP
#define Larva_HPP

#include "AgentEater.hpp"

class Larva: public AgentEater{
	public:
		Larva(int,int,int);
		Larva(int,int,int,Square*,bool);
		virtual void act() override;
		void display()override;
		bool isEvolve()const;
		virtual ~Larva()=default;
	protected:
		void evolve();
		int _evolveTurn;
		bool _isEvolve;
		bool _evolutionIfHungry;
};

#endif /* Larva_HPP */