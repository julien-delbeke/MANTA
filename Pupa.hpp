#ifndef Pupa_HPP
#define Pupa_HPP

#include "Egg.hpp"

class Pupa: public Egg{
	public:
		Pupa(int);
		Pupa(int,Square*);
		void display()override;
		virtual ~Pupa()=default;
} ;

#endif /* Pupa_HPP */