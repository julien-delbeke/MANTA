#include "Larva.hpp"

Larva::Larva(int turnHungry,int nbrDeathEating,int evolveTurn):AgentEater(0,0,turnHungry, nbrDeathEating,11), _evolveTurn(evolveTurn),_isEvolve(false),_evolutionIfHungry(false){
	_weight = 4;
}

Larva::Larva(int turnHungry,int nbrDeathEating,int evolveTurn,Square* pos,bool evolutionIfHungry):AgentEater(0,0,turnHungry, nbrDeathEating, pos,11),_evolveTurn(evolveTurn),_isEvolve(false),_evolutionIfHungry(evolutionIfHungry){
	_weight = 4;
	normalStimuli();
}

void Larva::act(){
	if(isAlive()){
		incTurn();
		if(!(_turnEating%getTurnHungry())){
			hungry();
		}
		if(_turn==_evolveTurn){
			evolve();
		}
	}
}


void Larva::display(){
	std::cout<<" La";
}

bool Larva::isEvolve()const{
	return _isEvolve;
}
void Larva::evolve(){
	if(!_evolutionIfHungry and isHungry()){
		_evolveTurn+=2;
	}
	else{
		_isEvolve=true;
	}
}