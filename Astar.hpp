#ifndef ASTAR_HPP
#define ASTAR_HPP

#include "Square.hpp"
#include <queue>
#include <map>

class Astar{
	public:
		Astar(Square*,Square*,int);
		Astar(const Astar&)=delete;
		Astar operator=(const Astar&)=delete;
		std::vector<Square*> getPath();
		~Astar()=default;
		class Node{
			public:
				Node(Square *,int);
				bool isSmal(const Node &)const;
				bool isEgal(const Node &)const;
				int getCout();
				Square* getSquare();
				~Node()=default;
			private:
				Square* _pos;
				int _cout;
		};
	private:
		void findPath(Square*,Square*,int);
		std::priority_queue<Node, std::vector<Node>, std::greater<Node>> _working;
		Square*_f;
		std::map<Square*,Square*> previousSquare;
};

int heuristic(Square*,Square*);
bool operator>(Astar::Node const&, Astar::Node const&);
bool operator<(Astar::Node const&, Astar::Node const&);
bool operator==(Astar::Node const&, Astar::Node const&);
bool operator!=(Astar::Node const&, Astar::Node const&);

#endif /* ASTAR_HPP */