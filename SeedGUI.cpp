#include "SeedGUI.hpp"

SeedGUI::SeedGUI(int weight,bool isMovable,int nbrPortion,std::string pictureName): Seed(weight,isMovable,nbrPortion),ObjectGUI(QString::fromStdString(pictureName)){
	_label=new QLabel();
	_label->setPixmap(_pic);
	_label->setStyleSheet("background:transparent;");
	_label->setScaledContents(true);
}
