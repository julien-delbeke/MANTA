#include "EggGUI.hpp"

EggGUI::EggGUI(QGridLayout* fenetre,int turnHatching,Square* myPosition, std::string eggImage): ObjectGUI(QString(eggImage.c_str())),Egg(turnHatching,myPosition){
    if(!_label){
	    _label = new QLabel();
	    _label->setFixedSize(SIZE,SIZE);
	    _label->setScaledContents(true);
	    _label->setStyleSheet("background:transparent;");
	    _label->setPixmap(_pic);
	    if(_myPosition){
	    	fenetre->addWidget(_label,_myPosition->getPosI(),_myPosition->getPosJ());
	    }
	}
}

void EggGUI::death(){
	_mtx.lock();
    Egg::death();
    emit deathSignal();

}

void EggGUI::deathSlot(){
    _namePix="skull.png";
    _pic=QPixmap(_namePix);
    _label->setPixmap(_pic);
    _mtx.unlock();
}