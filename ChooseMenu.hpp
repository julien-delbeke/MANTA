#ifndef CHOOSEMENU_HPP
#define CHOOSEMENU_HPP


#include "MenuGui.hpp"
#include <QApplication>
#include <QWidget>
#include <QTabWidget>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include <QRadioButton>
#include <QGroupBox>
#include <QGridLayout>
#include <QLayout>
#include <QPixmap>
#include <iostream>
#include <QMessageBox>
#include <QDesktopWidget>
#include <QMainWindow>



class chooseMenu : public QObject{

	Q_OBJECT

	public:
		chooseMenu();
		~chooseMenu();
		void launch_choosing();
        //int launch_simulation();
	public slots:
		void nonScientific();
		void Scientific();
	private:
        QWidget* fenetre;
        QMainWindow* mainWindow;

        QDesktopWidget *screen;
        QRect rect;

};

#endif