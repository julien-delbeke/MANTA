#ifndef _CLICKABLELABEL_HPP
#define _CLICKABLELABEL_HPP

#include <QLabel>

class ClickableLabel : public QLabel
{
Q_OBJECT
public:
    ClickableLabel();
    ~ClickableLabel();
signals:
    void clicked();
protected:
    void mousePressEvent(QMouseEvent* event);
 
};
#endif /* CLICKABLELABEL_HPP */