#include "World.hpp"

World::World(Map* map,int antIntel,int antTime,int eggTime,int larvaTime,int pupaTime,int antFrequenceMovement,int antFrequenceHungry,bool evolutionIfHungry,int larvaFrequenceHungry,int hungryResistanceLarva,int foodTraceLifetime,int ignoringTrace,int normalTraceLaps):_eggV(),_larvaV(),_pupaV(), _antV(),_other(),_map(map),_itEggV(),_itLarvaV(),_itPupaV(), _itAntV(),_itOther()
			,_queen(nullptr),_antIntel(antIntel),_antTime(antTime),_eggTime(eggTime),_larvaTime(larvaTime),_pupaTime(pupaTime),_antFrequenceMovement(antFrequenceMovement),_antFrequenceHungry(antFrequenceHungry),_evolutionIfHungry(evolutionIfHungry)
			,_larvaFrequenceHungry(larvaFrequenceHungry),_hungryResistanceLarva(hungryResistanceLarva),_stop(false),_fin(false),_foodTraceLifetime(foodTraceLifetime),_ignoringTrace(ignoringTrace),_normalTraceLaps(normalTraceLaps){
	_map->linkObject();
	
}
void World::management(){
	int j=0;
	std::random_device rd;
	std::mt19937 g(rd());
	int i=0;
	usleep(1000000);
	while((_eggV.size()||_larvaV.size()||_pupaV.size()||_antV.size() || _queen) and !_stop){
		oneTurn(g);
		i++;
		usleep(100000);//temps de l'animation des fourmis
		std::cout<<i<<std::endl;
		//_map->display();
		j=0;
		if(!_stop){
			while(_antV.begin()+j!=_antV.end()){
				while(!(*(_antV.begin()+j))->isReady()){
					usleep(100);//permet attendre le min et que tout les annimation soit fini
				
				}
				j++;
			}
			while(_queen and !_queen->isReady()){
				usleep(100);
			}
		}
	}
	std::cout<<"fin"<<std::endl;
	_fin=true;
}

void World::oneTurn(std::mt19937 g){
	int j=0;
	std::shuffle ( _eggV.begin(), _eggV.end(),g);
	Square* tempPos(nullptr);
	_itEggV=_eggV.begin();
	while(_itEggV!=_eggV.end()){
		(*_itEggV)->act();
		if((*_itEggV)->isHatching()){
			//createPupa(_pupaTime,(*_itEggV)->getPos());
			createLarva(_larvaFrequenceHungry,_hungryResistanceLarva,_larvaTime,(*_itEggV)->getPos(),_evolutionIfHungry);
			_other.push_back((*_itEggV));
			_eggV.erase(_itEggV);
		}
		else if(!(*_itEggV)->isAlive()){
			_other.push_back((*_itEggV));
			_eggV.erase(_itEggV);
		}
		else{
			_itEggV++;
		}
	}
	j=0;
	while(_eggV.begin()+j!=_eggV.end()){
		while(!(*(_eggV.begin()+j))->isReady()){
			usleep(100);//permet attendre le min et que tout les annimation soit fini
		}
		j++;
	}
	std::shuffle ( _larvaV.begin(), _larvaV.end(),g);
	_itLarvaV=_larvaV.begin();
	while(_itLarvaV!=_larvaV.end()){
		(*_itLarvaV)->act();
		if((*_itLarvaV)->isEvolve()){
			tempPos=(*_itLarvaV)->getPos();
			(*_itLarvaV)->toBeCarried();
			createPupa(_pupaTime,tempPos);
			deleteObj(*_itLarvaV);
			(*_itLarvaV)=nullptr;
			_larvaV.erase(_itLarvaV);
		}
		else if(!(*_itLarvaV)->isAlive()){
			_other.push_back((*_itLarvaV));
			_larvaV.erase(_itLarvaV);
		}
		else
			_itLarvaV++;
	}
	j=0;
	while(_larvaV.begin()+j!=_larvaV.end()){
		while(!(*(_larvaV.begin()+j))->isReady()){
			usleep(100);//permet attendre le min et que tout les annimation soit fini
		}
		j++;
	}
	std::shuffle ( _pupaV.begin(), _pupaV.end(),g);
	_itPupaV=_pupaV.begin();
	while(_itPupaV!=_pupaV.end()){
		(*_itPupaV)->act();
		if((*_itPupaV)->isHatching()){
			createAnt(_antIntel,_antFrequenceHungry,2,_antFrequenceMovement,_antTime,(*_itPupaV)->getPos(),_foodTraceLifetime,_ignoringTrace,_normalTraceLaps);/*a det*/
			_other.push_back((*_itPupaV));
			_pupaV.erase(_itPupaV);
		}
		else if(!(*_itPupaV)->isAlive()){
			_other.push_back((*_itPupaV));
			_pupaV.erase(_itPupaV);
		}
		else{
			_itPupaV++;
		}
	}
	j=0;
	while(_pupaV.begin()+j!=_pupaV.end()){
		while(!(*(_pupaV.begin()+j))->isReady()){
			usleep(100);//permet attendre le min et que tout les annimation soit fini
		}
		j++;
	}
	std::shuffle ( _antV.begin(), _antV.end(),g);
	_itAntV=_antV.begin();
	while(_itAntV!=_antV.end()){
		(*_itAntV)->act();
		if(!(*_itAntV)->isAlive()){
			_other.push_back((*_itAntV));
			_antV.erase(_itAntV);
		}
		else{
			_itAntV++;
		}
	}
	if(_queen){
		(_queen)->act();
		if(_queen->haveEgg()){
			int nbrNewEGG=_queen->getNewGeneration();
			for(int nbr=0;nbr<nbrNewEGG;nbr++){
				createEgg(_eggTime,_queen->getPos());
			}
		}
		if(!(_queen)->isAlive()){
			_other.push_back((_queen));
			_queen=nullptr;
		}
	}
	_map->aging();
}

void World::createAnt(int range,int turnHungry, int nbrDeathEating, int frequenMove,int turnDeath ,Square* Pos,int foodTraceLifetime,int ignoringTrace,int normalTraceLaps){
	_antV.push_back(new Ant(range,turnHungry,nbrDeathEating,frequenMove,turnDeath,Pos,foodTraceLifetime,ignoringTrace,normalTraceLaps));
}
void World::createEgg(int turnHatching,Square* myPosition){
	_eggV.push_back(new Egg(turnHatching,myPosition));
}
void World::createLarva(int turnHungry,int nbrDeathEating,int evolveTurn,Square* pos,bool evolutionIfHungry){
	_larvaV.push_back(new Larva(turnHungry,nbrDeathEating,evolveTurn,pos,evolutionIfHungry));
}
void World::createPupa(int turnHatching,Square* pos){
	_pupaV.push_back(new Pupa(turnHatching,pos));
}
void World::createQueen(int range,int turnHungry,int nbrDeathEating,int frequenMove,int turnDeath,int fregEgg,Square* Pos, std::string,int nbrEgg,int maxAlea){
	 _queen=new Queen(range,turnHungry,nbrDeathEating,frequenMove ,turnDeath,fregEgg,Pos,nbrEgg,maxAlea);
	
}
World::~World(){
	if(_map){
		delete _map;
		_map=nullptr;
	}

}
void World::deleteObj(Object* obj){
	delete obj;
}
bool World::isRuning(){
	return !_fin;
}