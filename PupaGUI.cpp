#include "PupaGUI.hpp"

PupaGUI::PupaGUI(QGridLayout* fenetre,int turnHatching,Square* myPosition, std::string pupaImage): ObjectGUI(QString(pupaImage.c_str())),Pupa(turnHatching,myPosition){
	if(!_label){
	    _label = new QLabel();
	    _label->setFixedSize(SIZE,SIZE);
	    _label->setScaledContents(true);
	    _label->setStyleSheet("background:transparent;");
	    _label->setPixmap(_pic);
	    if(_myPosition){
	   		fenetre->addWidget(_label,_myPosition->getPosI(),_myPosition->getPosJ());
	    }
	}
}

void PupaGUI::death(){
	_mtx.lock();
    Pupa::death();
    emit deathSignal();

}

void PupaGUI::deathSlot(){
    _namePix="skull.png";
    _pic=QPixmap(_namePix);
    _label->setPixmap(_pic);
    _mtx.unlock();
}