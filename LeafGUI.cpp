#include "LeafGUI.hpp"

LeafGUI::LeafGUI(int weight,bool isMovable,int nbrPortion,std::string pictureName): Leef(weight,isMovable,nbrPortion),ObjectGUI(QString::fromStdString(pictureName)){
	_label=new QLabel();
	_label->setPixmap(_pic);
	_label->setStyleSheet("background:transparent;");
	_label->setScaledContents(true);
}
