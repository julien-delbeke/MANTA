#ifndef PUPAGUI_HPP
#define PUPAGUI_HPP

#include <QApplication>
#include <QWidget>
#include <QGridLayout>
#include "ObjectGUI.hpp"
#include <QPixmap>
#include <iostream>
#include "Pupa.hpp"
#include "SquareGUI.hpp"
#define SIZE 15

class PupaGUI: public ObjectGUI,public Pupa {
	Q_OBJECT
	public:
		PupaGUI(QGridLayout*,int,Square*,std::string);
		virtual ~PupaGUI()=default;
	protected:
		virtual void death()override;
	public slots:
		void deathSlot();
	signals:
		void deathSignal();
};


#endif