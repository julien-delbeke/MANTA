#ifndef TRACE_HPP
#define TRACE_HPP

#include "Square.hpp"

class Trace{
	public:
		Trace(Square*,int,int);
		int getTime();
		void setType(int);
		void addLive(int);
		void aging();
		int getType();
		Square* getPos();
		~Trace()=default;
	private:
		void destruction();
		int _timeToLive;
		int _type;
		Square* _myPos;
};

#endif /* TRACE_HPP */