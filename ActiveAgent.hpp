#ifndef ACTIVEAGENT_HPP
#define ACTIVEAGENT_HPP

#define UP 0
#define UPR 1
#define RIGHT 2
#define DOWNR 3
#define DOWN 4
#define DOWNL 5
#define LEFT 6
#define UPL 7

#include "AgentEater.hpp"
#include "Astar.hpp"
#include <algorithm>    // std::random_shuffle
#include <vector>       // std::vector
#include <ctime>        // std::time
#include <cstdlib>      // std::rand, std::srand
#include <random>

class ActiveAgent: public AgentEater {
public:
    ActiveAgent(int, int, int, int, int, int, int);
    ActiveAgent(int, int, int, int, int, int, Square *, int);
    ActiveAgent(const ActiveAgent &) = delete;
    ActiveAgent &operator=(const ActiveAgent &) = delete;
    virtual ~ActiveAgent() = default;
protected:
    int _frequenMove;
    int _goal;
    int _turnDeath;
    Square *_lastSquare;
    std::vector<int> _movePos;
    std::vector<Square *> _path;
    static std::random_device _rd;
    static std::mt19937 _g;
    virtual void randomMove();
    virtual bool moveIsOk(Square *);
    virtual bool moveSquare(Square *);
    virtual bool moveAway(Square *);
    virtual bool inARoom(int, int = 150);
    virtual bool haveNWall(Square *, int);
    virtual Square *findWallOfRoom();
    virtual void goToWall();
    virtual void goToObjective();
    virtual void moveAwayDoor();
    virtual bool InBack(Square *, int);
    virtual bool InFront(Square *, int);
    virtual int typeLastMove();
    virtual bool pathInBAck(Square *, int);
} ;

#endif /* ACTIVEAGENT_HPP */
