#ifndef Object_h
#define Object_h

#include <iostream>
#include "Stimuli.hpp"
class Square;

class Object{
public:
	Object(int,bool,int,int);
	Object(bool); //Utilisé dans le cas si objet ne peux être bougé.
	Object(int,bool,Square*,int,int);
	Object(const Object&)=delete;
	Object& operator=(const Object&)=delete;
	virtual int getNumberToMove();
	virtual bool getMovability();
	virtual Square* getPos();
	virtual void setPos(Square*);
	virtual void toBeCarried();
	virtual void putOn(Square*);
	virtual int getWeight();
	virtual ~Object();
	virtual void display()=0;
	virtual void setStimuliType(int);
	virtual void incStimuliType();
	virtual void squareFreeStimuli(int);
protected:
	virtual void normalStimuli();
	int _numberToMove;
	bool _movable;
	Square* _myPosition;
	std::weak_ptr<Stimuli> _rootStimuli;
	int _weight;
	int _stimuliType;
	float _stimuliPower;
};
#endif