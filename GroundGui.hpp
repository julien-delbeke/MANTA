#ifndef GroundGui_h
#define GroundGui_h

#include "Wall.hpp"
#include <QApplication>
#include <QWidget>
#include <QTabWidget>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include <QRadioButton>
#include <QGroupBox>
#include <QGridLayout>
#include <QLayout>
#include <iostream>
#include "Ground.hpp"

class GroundGui : public Ground{
public:
	GroundGui(bool);
	QLabel* returnImage(int, int);
	~GroundGui()=default;
private:
	std::string _ground_img;
	QLabel* _label;
};
#endif