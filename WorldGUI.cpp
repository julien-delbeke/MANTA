#include "WorldGUI.hpp"

WorldGUI::WorldGUI(MapGUI* map,std::string antPic,std::string eggPic,std::string larvaPic,std::string pupaPic,int antIntel,int antTime,int eggTime,int larvaTime,int pupaTime,int antFrequenceMovement,int antFrequenceHungry,bool evolutionIfHungry,int larvaFrequenceHungry, int hungryResistanceLarva,int foodTraceLifetime,int ignoringTrace, int normalTraceLaps):QObject(),
			World(map,antIntel,antTime,eggTime,larvaTime,pupaTime,antFrequenceMovement,antFrequenceHungry,evolutionIfHungry,larvaFrequenceHungry,hungryResistanceLarva,foodTraceLifetime,ignoringTrace,normalTraceLaps),_map(map),_mtx(),_antPic(antPic),_eggPic(eggPic),_larvaPic(larvaPic),_pupaPic(pupaPic){
	QObject::connect(this,SIGNAL(createAntSignal(int,int,int,int,int,Square*,int,int,int)),this,SLOT(createAntSlot(int,int,int,int,int,Square*,int,int,int)));
	QObject::connect(this,SIGNAL(createEggSignal(int,Square*)),this,SLOT(createEggSlot(int,Square*)));
	QObject::connect(this,SIGNAL(createLarvaSignal(int,int,int,Square*,bool)),this,SLOT(createLarvaSlot(int,int,int,Square*,bool)));
	QObject::connect(this,SIGNAL(createPupaSignal(int,Square*)),this,SLOT(createPupaSlot(int,Square*)));
	QObject::connect(this,SIGNAL(createQueenSignal(int,int,int,int,int,int,Square*,std::string,int,int)),this,SLOT(createQueenSlot(int,int,int,int,int,int,Square*,std::string,int,int)));
	QObject::connect(_map,SIGNAL(createLeafSignal(int,bool,int,std::string,Square*)),this,SLOT(createLeafSlot(int,bool,int,std::string,Square*)));
	QObject::connect(_map,SIGNAL(createSeedSignal(int,bool,int,std::string,Square*)),this,SLOT(createSeedSlot(int,bool,int,std::string,Square*)));
	QObject::connect(this,SIGNAL(createLeafSignal(int,bool,int,std::string,Square*)),this,SLOT(createLeafSlot(int,bool,int,std::string,Square*)));
	QObject::connect(this,SIGNAL(createSeedSignal(int,bool,int,std::string,Square*)),this,SLOT(createSeedSlot(int,bool,int,std::string,Square*)));
	QObject::connect(this,SIGNAL(destruction(Object*)),this,SLOT(destructionSlot(Object*)));
}

void WorldGUI::oneTurn(std::mt19937 g){
	_mtx.lock();
	World::oneTurn(g);
	_mtx.unlock();
}
void WorldGUI::createAnt(int range,int turnHungry, int nbrDeathEating, int frequenMove,int turnDeath ,Square* Pos,int foodTraceLifetime,int ignoringTrace,int normalTraceLaps){
	emit createAntSignal(range,turnHungry,nbrDeathEating,frequenMove,turnDeath,Pos,foodTraceLifetime,ignoringTrace,normalTraceLaps);
}
void WorldGUI::createEgg(int turnHatching,Square* myPosition){
	emit createEggSignal(turnHatching,myPosition);
}
void WorldGUI::createLarva(int turnHungry,int nbrDeathEating,int evolveTurn,Square* pos,bool evolutionIfHungry){
	emit createLarvaSignal(turnHungry,nbrDeathEating,evolveTurn,pos,evolutionIfHungry);
}
void WorldGUI::createPupa(int turnHatching,Square* pos){
	emit createPupaSignal(turnHatching,pos);
}
void WorldGUI::createLeaf(int weight,bool isMovable,int nbrPortion,std::string pictureName,Square* pos){
	emit createLeafSignal(weight,isMovable,nbrPortion,pictureName,pos);
}
void WorldGUI::createSeed(int weight,bool isMovable,int nbrPortion,std::string pictureName,Square* pos){
	emit createSeedSignal(weight,isMovable,nbrPortion,pictureName,pos);
}
void WorldGUI::createAntSlot(int range,int turnHungry, int nbrDeathEating, int frequenMove,int turnDeath ,Square* Pos,int foodTraceLifetime,int ignoringTrace,int normalTraceLaps){
	_mtx.lock();
	AntGUI *ant = new AntGUI(_map->getGrid(),range,turnHungry,nbrDeathEating,frequenMove,turnDeath,Pos,_antPic,foodTraceLifetime,ignoringTrace,normalTraceLaps);
    QObject::connect(ant,SIGNAL(moveSignal(Square*)),ant,SLOT(moveSlot(Square*)));
    QObject::connect(ant,SIGNAL(takeObjectSignal(Object*)),ant,SLOT(takeObjectSlot(Object*)));
    QObject::connect(ant,SIGNAL(normalSkinSignal()),ant,SLOT(normalSkinSlot()));
    QObject::connect(ant,SIGNAL(deathSignal()),ant,SLOT(deathSlot()));
    QObject::connect(ant,SIGNAL(destruction(Object*)),ant,SLOT(destructionSlot(Object*)));
    _antV.push_back(ant);
	_mtx.unlock();
}
void WorldGUI::createEggSlot(int turnHatching,Square* myPosition){
	_mtx.lock();
	EggGUI* egg=new EggGUI(_map->getGrid(),turnHatching,myPosition,_eggPic);
	QObject::connect(egg,SIGNAL(deathSignal()),egg,SLOT(deathSlot()));
	_eggV.push_back(egg);
	_mtx.unlock();
}
void WorldGUI::createLarvaSlot(int turnHungry,int nbrDeathEating,int evolveTurn,Square* pos,bool evolutionIfHungry){
	_mtx.lock();
	LarvaGUI *larva=new LarvaGUI(_map->getGrid(),turnHungry,nbrDeathEating,evolveTurn,pos,_larvaPic,evolutionIfHungry);
	QObject::connect(larva,SIGNAL(deathSignal()),larva,SLOT(deathSlot()));
	_larvaV.push_back(larva);
	_mtx.unlock();
}
void WorldGUI::createPupaSlot(int turnHatching,Square* pos){
	_mtx.lock();
	PupaGUI *pupa=new PupaGUI(_map->getGrid(),turnHatching,pos,_pupaPic);
	QObject::connect(pupa,SIGNAL(deathSignal()),pupa,SLOT(deathSlot()));
	_pupaV.push_back(pupa);;
	_mtx.unlock();
}
void WorldGUI::createLeafSlot(int weight,bool isMovable,int nbrPortion,std::string pictureName,Square* pos){
	if(pos){
		_mtx.lock();
		LeafGUI *leafTemp = new LeafGUI(weight,isMovable,nbrPortion,pictureName);
	   	if(pos->addObjectOnSquare(leafTemp))
	    	leafTemp->setPos(pos);
		_mtx.unlock();
	}
}
void WorldGUI::createSeedSlot(int weight,bool isMovable,int nbrPortion,std::string pictureName,Square* pos){
	if(pos){
		_mtx.lock();
	    SeedGUI *seedTemp = new SeedGUI(weight,isMovable,nbrPortion,pictureName);
	    if(pos->addObjectOnSquare(seedTemp))
	    	seedTemp->setPos(pos);
		_mtx.unlock();
	}
}
void WorldGUI::createQueen(int range,int turnHungry,int nbrDeathEating,int frequenMove ,int turnDeath,int fregEgg,Square* Pos, std::string queenImage,int nbrEgg,int maxAlea){
	emit createQueenSignal(range,turnHungry,nbrDeathEating,frequenMove,turnDeath,fregEgg,Pos,queenImage,nbrEgg,maxAlea);
}
void WorldGUI::createQueenSlot(int range,int turnHungry,int nbrDeathEating,int frequenMove,int turnDeath,int fregEgg,Square* Pos, std::string queenImage,int nbrEgg,int maxAlea){
	QueenGUI* queen=new QueenGUI(_map->getGrid(),range,turnHungry,nbrDeathEating,frequenMove ,turnDeath,fregEgg,Pos,queenImage,nbrEgg,maxAlea);
	QObject::connect(queen,SIGNAL(moveSignal(Square*)),queen,SLOT(moveSlot(Square*)));
	QObject::connect(queen,SIGNAL(deathSignal()),queen,SLOT(deathSlot()));
	_queen=queen;
}
void WorldGUI::destructionSlot(Object* obj){
	delete obj;
}
void WorldGUI::deleteObj(Object* obj){
	emit destruction(obj);
}
void WorldGUI::stop(){
	_stop=true;
}
