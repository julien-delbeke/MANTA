#include "Pupa.hpp"

Pupa::Pupa(int turnHatching): Egg(turnHatching,11){
	_weight = 8;
}

Pupa::Pupa(int turnHatching,Square* pos): Egg(turnHatching,pos,11){
	_weight = 8;
}

void Pupa::display(){
	if(_isAlive)
		std::cout<<" P ";
	else{
		std::cout<<"MOR";
	}
}
