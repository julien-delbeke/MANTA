#include "Stimuli.hpp"
#include "Square.hpp"

Stimuli::Stimuli(int type,float power,Square* origin,Object* root): _type(type),_power(power),_origin(origin),_root(root),_myPropagation(){}

Stimuli::Stimuli(Stimuli& o,Square* myPos):_type(o._type),_power((o._power-myPos->getSimuliResi()<0?0:o._power-myPos->getSimuliResi())),_origin(myPos),_root(o._root),_myPropagation(){}

Stimuli::Stimuli(Stimuli* o):_type(o->_type),_power(o->_power),_origin(o->_origin),_root(o->_root),_myPropagation(){}

Stimuli::Stimuli():_type(NORMAL),_power(0),_origin(nullptr),_root(nullptr),_myPropagation(){}

bool Stimuli::isBiger(Stimuli const& b)const{
	return _power>b._power;
}
bool Stimuli::isEgal(Stimuli const& b)const{
	return _power==b._power;
}


bool operator>(Stimuli const& a, Stimuli const& b){
	return a.isBiger(b);
}
bool operator>=(Stimuli const& a, Stimuli const& b){
	return a.isBiger(b) or a.isEgal(b);
}
Object* Stimuli::getRoot()const{
	return _root;
}
int Stimuli::getType()const{
	return _type;
}
Square* Stimuli::getOrinin()const{
	return _origin;
}
void Stimuli::propagationStimuli(std::shared_ptr<Stimuli> current){
	if(_origin){
		if(_power)
			propagationStimuliWork(current);
		else
			current->getOrinin()->addStimuli(current);
	}
}

void Stimuli::propagationStimuliWork(std::shared_ptr<Stimuli> current){
	if(getPower()){
		Square* next(nullptr);
		std::queue<std::shared_ptr<Stimuli>> all;
		std::weak_ptr<Stimuli> currentWeak;	
		all.push(current);
		std::shared_ptr<Stimuli> currentShare;
		std::shared_ptr<Stimuli> temp;
		while(all.size()){
			if(all.front()->getPower()){
				all.front()->getOrinin()->addStimuli(all.front());
			}
			currentWeak=all.front();
			all.pop();
			currentShare=currentWeak.lock();
			if(currentShare and currentShare->getPower()>0){
				for(int i=0;i<8;i++){
					next=currentShare->getOrinin()->getNeighbor(i);
					if(next and next->canGoIn(0)){
						temp=std::make_shared<Stimuli>(new Stimuli(*currentShare,next));
						currentShare->_myPropagation.push_back(temp);
						all.push(temp);
					}
				}
				temp=std::shared_ptr<Stimuli>();
			}
		}
	}
}

float Stimuli::getPower()const{
	return _power;
}

void Stimuli::display(){
	std::cout<<"D display"<<std::endl;
	std::cout<<"Stimuli: type"<<_type<<" force "<<_power<<" Position " <<std::endl;
	if(_origin)
		std::cout<<"a une pos";
	else
		std::cout<<"problem";
	std::cout<<"Racine ";
	if(_root)
		std::cout<<"en a une "<<std::endl;
	else
		std::cout<<"problem"<<std::endl;
	std::cout<<"F display"<<std::endl;
}

void Stimuli::remove(){
	Square* next(nullptr);
	_origin->removeStimuli(this);
	for(int i=0;i<8;i++){
		next=_origin->getNeighbor(i);
		if(next and next->canGoIn(0)){
			next->endStimuli(this);
		}
	}
	if(_myPropagation.size()){
		auto it=_myPropagation.begin();
		while (it!=_myPropagation.end()){
			auto rPropagation=(*it).lock();
			if(rPropagation){
				rPropagation->remove();
			}
			it++;
		}
	}
}
Stimuli::~Stimuli(){
}
bool Stimuli::operator!=(const Stimuli& o){
	return _type!=o._type and _power!=o._power and _origin!=o._origin and _root!=o._root ;
}
bool Stimuli::notSameRoot(Stimuli* o){
	return _type==o->_type and _root!=o->_root;
}