#include "AntGUI.hpp"

AntGUI::AntGUI(QGridLayout* fenetre,int range,int turnHungry, int nbrDeathEating, int frequenMove,int turnDeath ,Square* Pos, std::string antImage,int foodTraceLifetime,int ignoringTrace,int normalTraceLaps): ObjectGUI(QString(antImage.c_str())),
                        Ant(range,turnHungry,nbrDeathEating,frequenMove,turnDeath,Pos,foodTraceLifetime,ignoringTrace,normalTraceLaps),anim(nullptr),grid(fenetre),_annimeDone(true),_putObject(false){
    _label = new QLabel();
    _label->setFixedSize(SIZE,SIZE);
    _label->setScaledContents(true);
    _label->setStyleSheet("background:transparent;");
    _label->setPixmap(_pic);
    fenetre->addWidget(_label,_myPosition->getPosI(),_myPosition->getPosJ());

}

bool AntGUI::moveSquare(Square* next){
    if(Ant::moveSquare(next)){
        _annimeDone=false;
        emit moveSignal(next);
        return true;
    }
    return false;
}

void AntGUI::moveSlot(Square* next){
    if(!anim){
        anim = new QPropertyAnimation(getLabel(),"geometry");
    }
    SquareGUI* temp2= dynamic_cast<SquareGUI*>(next);
    if(temp2){
        QLabel *temp =temp2->getLabel();
        if(temp){
            if(_label and grid->indexOf(_label)){
                delete grid->takeAt(grid->indexOf(_label));
            }
            anim->setStartValue(QRect(_label->x(),_label->y(),0,0));
            anim->setEndValue(QRect(temp->x()+1,temp->y()+1,0,0));
            anim->setDuration(DURATION);
            QObject::connect(anim,SIGNAL(finished()),this,SLOT(endAnimation()));
            anim->start();
        }
    }
}


void AntGUI::endAnimation(){
    if(_myPosition){
        grid->addWidget(_label,_myPosition->getPosI(),_myPosition->getPosJ());
    }
    if(_putObject){
        _putObject=false;
        _label->setPixmap(_pic);
    }
    delete anim;
    anim=nullptr;
    _annimeDone=true;
}
bool AntGUI::isReady(){
    return _annimeDone;

}
void AntGUI::takeObject(Object* toBeCarried){
    Ant::takeObject(toBeCarried);
    emit takeObjectSignal(toBeCarried);
}

void AntGUI::putObject(Square * position){
    Ant::putObject(position);
    _putObject=true;
}
void  AntGUI::takeObjectSlot(Object* toBeCarried){
    auto temp=dynamic_cast<ObjectGUI*>(toBeCarried);
    if(temp){
       _label->setPixmap(QPixmap(temp->getPixname()));
    }
}

void AntGUI::normalSkinSlot(){
    _label->setPixmap(_pic);
    _annimeDone=true;
    
}

void AntGUI::feed(AgentEater* target){
    Ant::feed(target);
    if(!_carry){
        if(!_annimeDone){
            _putObject=true;
        }
        else{
            emit normalSkinSignal();
            _annimeDone=false;
        }
    }
}

void AntGUI::death(){
    Ant::death();
    emit deathSignal();

}

void AntGUI::deathSlot(){
    _namePix="skull.png";
    _pic=QPixmap(_namePix);
    _label->setPixmap(_pic);
}
void AntGUI::deletFood(Object* obj){
    emit destruction(obj);
}
void AntGUI::destructionSlot(Object* obj){
    delete obj;
}