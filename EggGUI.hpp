#ifndef EGGGUI_HPP
#define EGGGUI_HPP

#include <QApplication>
#include <QWidget>
#include <QGridLayout>
#include "ObjectGUI.hpp"
#include <QPixmap>
#include <iostream>
#include "Egg.hpp"
#include "SquareGUI.hpp"
#define SIZE 15

class EggGUI: public ObjectGUI,public Egg {
	Q_OBJECT
	public:
		EggGUI(QGridLayout*,int,Square*,std::string);
		virtual ~EggGUI()=default;
	protected:
		virtual void death()override;
	public slots:
		void deathSlot();
	signals:
		void deathSignal();
};


#endif