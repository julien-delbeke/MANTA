#include "Trace.hpp"

Trace::Trace(Square* myPos,int timeToLive,int type) :_timeToLive(timeToLive),_type(type),_myPos(myPos){
	if(myPos and timeToLive){
		Trace* temp(myPos->getTrace());
		if(!temp)	
			_myPos->addTrace(this);
		else{
			if(type){
				if(type==temp->getType()){
					temp->addLive(timeToLive/2);
				}
				if(type>temp->getType()){
					temp->setType(std::max(_type,temp->getType()));
					temp->addLive(timeToLive-temp->getTime());
				}	
			}
			else{
				if(!temp->getType() and  temp->getTime()<timeToLive/4)
					temp->addLive(timeToLive-temp->getTime());
			}
		}
	}
}

int Trace::getTime(){
	return _timeToLive;
}
int Trace::getType(){
	return _type;
}
Square* Trace::getPos(){
	return _myPos;
}	
void Trace::setType(int type){
	_type=type;
}	
void Trace::aging(){
	_timeToLive--;
	if(!_timeToLive){
		destruction();
	}
}
void Trace::destruction(){
	_myPos->removeTrace(this);
}
void Trace::addLive(int live){
	if(_timeToLive>500){
		_timeToLive+=live/5;
	}
	else{
		_timeToLive+=live;
	}
}
