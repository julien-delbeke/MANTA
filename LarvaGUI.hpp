#ifndef LARVAGUI_HPP
#define LARVAGUI_HPP

#include <QApplication>
#include <QWidget>
#include <QGridLayout>
#include "ObjectGUI.hpp"
#include <QPixmap>
#include <iostream>
#include "Larva.hpp"
#include "SquareGUI.hpp"
#define SIZE 15

class LarvaGUI: public ObjectGUI,public Larva {
	Q_OBJECT
	public:
		LarvaGUI(QGridLayout*,int,int,int,Square*,std::string,bool);
		virtual ~LarvaGUI()=default;
	private:
		virtual void death()override;
	public slots:
		void deathSlot();
	signals:
		void deathSignal();
};



#endif