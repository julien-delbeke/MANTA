#ifndef World_HPP
#define World_HPP

#include "Egg.hpp"
#include "Larva.hpp"
#include "Pupa.hpp"
#include "Ant.hpp"
#include "Leef.hpp"
#include "Map.hpp"
#include "Queen.hpp"
#include <unistd.h>

class World{
	public:
		World(Map*,int,int,int,int,int,int,int,bool,int,int,int,int,int);
		World(const World&)=delete;
		World operator=(const World&)=delete;
		void management();
		virtual void createAnt(int,int,int,int,int,Square*,int,int,int);
		virtual void createEgg(int,Square*);
		virtual void createLarva(int,int,int,Square*,bool);
		virtual void createPupa(int,Square*);
		virtual void createQueen(int,int,int,int,int,int,Square*,std::string,int=8,int=2);
		bool isRuning();
		virtual ~World();
	protected:
		std::vector<Egg*> _eggV;
		std::vector<Larva*> _larvaV;
		std::vector<Pupa*> _pupaV;
		std::vector<Ant*> _antV;
		std::vector<Object*> _other;
		virtual void oneTurn(std::mt19937);
		virtual void deleteObj(Object*);
	private:
		Map* _map;
		std::vector<Egg*>::iterator _itEggV;
		std::vector<Larva*>::iterator _itLarvaV;
		std::vector<Pupa*>::iterator _itPupaV;
		std::vector<Ant*>::iterator _itAntV;
		std::vector<Object*>::iterator _itOther;
	protected:
		Queen* _queen;
		int _antIntel;
        int _antTime;
        int _eggTime;
        int _larvaTime;
        int _pupaTime;
        int _antFrequenceMovement;
        int _antFrequenceHungry;
        bool _evolutionIfHungry;
        int _larvaFrequenceHungry;
        int _hungryResistanceLarva;
        bool _stop;
        bool _fin;
        int _foodTraceLifetime;
        int _ignoringTrace;
        int _normalTraceLaps;
		

};




#endif /* World_HPP */