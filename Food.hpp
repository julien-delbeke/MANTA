#ifndef Food_h
#define Food_h

#include "Object.hpp"
#include "Stimuli.hpp"
#include "Square.hpp"
#include <memory>


class Food : public Object{
public:
	Food(int,bool,int);
	virtual int getPortion()const;
	virtual void beEaten();
	virtual void setPos(Square*)override;
	virtual void toBeCarried()override;
	virtual void putOn(Square*)override;
	virtual void incStimuliType()override;
	virtual ~Food()=default;
protected:
	int _portion;
	float _powerNormal;

};
#endif