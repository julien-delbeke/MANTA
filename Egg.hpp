#ifndef EGG_HPP
#define EGG_HPP

#include "Agent.hpp"

class Egg: public Agent{
	public:
		Egg(int,int=7);
		Egg(int,Square*,int=7);
		virtual void act() override;
		virtual bool isHatching()const;
		virtual void display()override;
		//Destructor
		virtual ~Egg()=default;
	protected:
		int _turnHatching;
		bool _hatching;
		virtual void hatching();
		
};

#endif /* EGG_HPP */